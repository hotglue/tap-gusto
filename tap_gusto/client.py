"""REST client handling, including gustoStream base class."""

import logging
from typing import Any, Dict, Optional

import requests
from singer_sdk.streams import RESTStream

from tap_gusto.auth import OAuth2Authenticator

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class gustoStream(RESTStream):
    """gusto stream class."""

    records_jsonpath = "$[*]"

    @property
    def url_base(self) -> str:
        if self.config.get("is_demo"):
            return "https://api.gusto-demo.com"
        return "https://api.gusto.com"

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        url = f"{self.url_base}/oauth/token"
        return OAuth2Authenticator(self, self._tap.config_file, url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        # Get the total pages header
        total_pages = int(response.headers.get("X-Total-Pages"))
        page = int(response.headers.get("X-Page"))
        if page >= total_pages:
            return None
        return page + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}

        if next_page_token:
            params["page"] = next_page_token
        else:
            params["page"] = 1

        return params
