"""Stream type classes for tap-gusto."""

from typing import Optional

from singer_sdk import typing as th

from tap_gusto.client import gustoStream


class CompaniesStream(gustoStream):
    """Define Companies Stream"""

    name = "companies"
    path = "/v1/companies"
    primary_keys = ["ein"]
    schema = th.PropertiesList(
        th.Property("ein", th.StringType),
        th.Property("entity_type", th.StringType),
        th.Property("is_suspended", th.BooleanType),
        th.Property("company_status", th.StringType),
        th.Property("id", th.IntegerType),
        th.Property("uuid", th.StringType),
        th.Property("name", th.StringType),
        th.Property("trade_name", th.StringType),
        th.Property("tier", th.StringType),
        th.Property("is_partner_managed", th.BooleanType),
        th.Property(
            "locations",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("street_1", th.StringType),
                    th.Property("street_2", th.StringType),
                    th.Property("city", th.StringType),
                    th.Property("state", th.StringType),
                    th.Property("zip", th.StringType),
                    th.Property("country", th.StringType),
                    th.Property("active", th.BooleanType),
                )
            ),
        ),
        th.Property(
            "compensations",
            th.PropertiesList(
                th.Property(
                    "hourly",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("name", th.StringType),
                            th.Property("multiple", th.NumberType),
                        )
                    ),
                ),
                th.Property(
                    "fixed",
                    th.ArrayType(th.ObjectType(th.Property("name", th.StringType))),
                ),
                th.Property(
                    "paid_time_off",
                    th.ArrayType(th.ObjectType(th.Property("name", th.StringType))),
                ),
            ),
        ),
        th.Property(
            "primary_signatory",
            th.PropertiesList(
                th.Property("uuid", th.StringType),
                th.Property("first_name", th.StringType),
                th.Property("middle_initial", th.StringType),
                th.Property("uuid", th.StringType),
                th.Property("uuid", th.StringType),
                th.Property("uuid", th.StringType),
                th.Property(
                    "home_address",
                    th.PropertiesList(
                        th.Property("street_1", th.StringType),
                        th.Property("street_2", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("state", th.StringType),
                        th.Property("zip", th.StringType),
                        th.Property("country", th.StringType),
                    ),
                ),
            ),
        ),
        th.Property(
            "primary_payroll_admin",
            th.PropertiesList(
                th.Property("first_name", th.StringType),
                th.Property("last_name", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("email", th.StringType),
            ),
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionart for child streams."""

        return {"company_id": record["id"]}


class ContractorsStream(gustoStream):
    """Define a contractors stream."""

    name = "contractors"
    path = "/v1/companies/{company_id}/contractors"
    primary_keys = ["uuid"]
    parent_stream_type = CompaniesStream

    schema = th.PropertiesList(
        th.Property("uuid", th.StringType),
        th.Property("id", th.IntegerType),
        th.Property("company_uuid", th.StringType),
        th.Property("company_id", th.IntegerType),
        th.Property("wage_type", th.StringType),
        th.Property("version", th.StringType),
        th.Property("type", th.StringType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("middle_initial", th.StringType),
        th.Property("business_name", th.StringType),
        th.Property("ein", th.StringType),
        th.Property("email", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("is_active", th.BooleanType),
        th.Property(
            "address",
            th.PropertiesList(
                th.Property("street_1", th.StringType),
                th.Property("street_2", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("country", th.StringType),
            ),
        ),
        th.Property("hourly_rate", th.StringType),
    ).to_dict()


class EmployeesStream(gustoStream):
    """Define custom stream."""

    name = "employees"
    path = "/v1/companies/{company_id}/employees"
    primary_keys = ["id"]
    parent_stream_type = CompaniesStream

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("uuid", th.StringType),
        th.Property("first_name", th.StringType),
        th.Property("middle_initial", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("comapany_id", th.IntegerType),
        th.Property("comapany_uuid", th.StringType),
        th.Property("maneger_id", th.IntegerType),
        th.Property("maneger_uuid", th.StringType),
        th.Property("version", th.StringType),
        th.Property("current_employment_status", th.StringType),
        th.Property("onboarding_status", th.StringType),
        th.Property("department", th.StringType),
        th.Property("terminated", th.BooleanType),
        th.Property("two_percent_shareholder", th.BooleanType),
        th.Property("onboarded", th.BooleanType),
        th.Property("has_ssn", th.BooleanType),
        th.Property(
            "jobs",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("uuid", th.StringType),
                    th.Property("version", th.StringType),
                    th.Property("employee_id", th.IntegerType),
                    th.Property("employee_uuid", th.StringType),
                    th.Property("current_compensation_id", th.IntegerType),
                    th.Property("current_compensation_uuid", th.StringType),
                    th.Property("payment_unit", th.StringType),
                    th.Property("primary", th.BooleanType),
                    th.Property("two_percent_shareholder", th.BooleanType),
                    th.Property("title", th.StringType),
                    th.Property(
                        "compensations",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("id", th.IntegerType),
                                th.Property("uuid", th.StringType),
                                th.Property("version", th.StringType),
                                th.Property("payment_unit", th.StringType),
                                th.Property("flsa_status", th.StringType),
                                th.Property("job_id", th.IntegerType),
                                th.Property("adjust_for_minimum_wage", th.BooleanType),
                                th.Property("job_uuid", th.StringType),
                                th.Property("effective_date", th.DateTimeType),
                                th.Property("rate", th.StringType),
                            )
                        ),
                    ),
                    th.Property("rate", th.StringType),
                    th.Property("ire_date", th.DateTimeType),
                    th.Property("location_id", th.IntegerType),
                    th.Property(
                        "location",
                        th.ObjectType(
                            th.Property("id", th.IntegerType),
                            th.Property("street_1", th.StringType),
                            th.Property("street_2", th.StringType),
                            th.Property("city", th.StringType),
                            th.Property("state", th.StringType),
                            th.Property("zip", th.StringType),
                            th.Property("country", th.StringType),
                            th.Property("inactive", th.BooleanType),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "eligible_paid_time_off",
            th.ArrayType(
                th.ObjectType(
                    th.Property("name", th.StringType),
                    th.Property("accrual_unit", th.StringType),
                    th.Property("accrual_rate", th.StringType),
                    th.Property("accrual_period", th.StringType),
                    th.Property("accrual_balance", th.StringType),
                    th.Property("maximum_accrual_balance", th.StringType),
                    th.Property("paid_at_termination", th.BooleanType),
                )
            ),
        ),
        th.Property(
            "terminations",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("uuid", th.StringType),
                    th.Property("version", th.StringType),
                    th.Property("employee_id", th.IntegerType),
                    th.Property("employee_uuid", th.StringType),
                    th.Property("active", th.BooleanType),
                    th.Property("effective_date", th.DateTimeType),
                    th.Property("run_termination_payroll", th.StringType),
                )
            ),
        ),
        th.Property(
            "home_address",
            th.PropertiesList(
                th.Property("id", th.IntegerType),
                th.Property("version", th.StringType),
                th.Property("company_id", th.IntegerType),
                th.Property("employee_id", th.IntegerType),
                th.Property("phone_number", th.StringType),
                th.Property("street_1", th.StringType),
                th.Property("street_2", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("country", th.StringType),
                th.Property("active", th.BooleanType),
                th.Property("mailing_address", th.BooleanType),
                th.Property("filling_address", th.BooleanType),
            ),
        ),
        th.Property(
            "garnishments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("version", th.StringType),
                    th.Property("employee_id", th.IntegerType),
                    th.Property("active", th.BooleanType),
                    th.Property("amount", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("court_order", th.BooleanType),
                    th.Property("times", th.IntegerType),
                    th.Property("recurring", th.BooleanType),
                    th.Property("annual_maximum", th.StringType),
                    th.Property("pay_period_maximum", th.StringType),
                    th.Property("deduct_as_percentage", th.BooleanType),
                )
            ),
        ),
        th.Property("date_of_birth", th.DateTimeType),
        th.Property("ssn", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("preferred_first_name", th.StringType),
        th.Property("work_email", th.StringType),
        th.Property("current_employment_status", th.StringType),
    ).to_dict()
