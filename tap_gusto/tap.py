"""gusto tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_gusto.streams import CompaniesStream, ContractorsStream, EmployeesStream

STREAM_TYPES = [
    CompaniesStream,
    EmployeesStream,
    ContractorsStream,
]


class Tapgusto(Tap):
    """gusto tap class."""

    name = "tap-gusto"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property("access_token", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("is_demo", th.BooleanType, default=False),
        th.Property("redirect_uri", th.StringType, required=True),
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapgusto.cli()
